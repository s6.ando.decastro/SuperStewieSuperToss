﻿using UnityEngine;
using System.Collections;

public class DataManager : MonoInstance<DataManager> {
	
	// Use this for initialization
	void Start () {
		DontDestroyOnLoad (gameObject);
	}
	
	#region PAUSE
	private bool _isPause = false;
	public bool isPause{
		get{
			return _isPause;
		}
		set{
			_isPause = value;
			//-- pause everything here!!
		}
	}
	#endregion

	#region IN GAME DATA

	public void resetToStart(){
		isPause = false;
		gameLevel = 1;
		currentScore = 0;
	}

	public bool gameIsStarted = false;
	public int gameLevel = 1; //-- 1 to 3
	public int currentScore = 0;

	#endregion

	//-----------------------------------------

	#region SAVE KEY
	const string SAVE_BGM  = "save_bgmIsOn";
	const string SAVE_SFX  = "save_sfxIsOn";
	const string SAVE_BESTSCORE  = "save_bestScore";
	const string SAVE_LASTSCORE  = "save_lastScore";
	#endregion

	#region SAVE DATA

	private int _bgmIsOn;
	public bool bgmIsOn{
		get{
			if (PlayerPrefs.HasKey (SAVE_BGM)) {
				_bgmIsOn = PlayerPrefs.GetInt (SAVE_BGM);
			} else {
				_bgmIsOn = 1;
				PlayerPrefs.SetInt (SAVE_BGM, _bgmIsOn);
			}
			return (_bgmIsOn > 0);
		}
		set{
			if (value) {
				_bgmIsOn = 1;
			} else {
				_bgmIsOn = 0;
			}
			PlayerPrefs.SetInt (SAVE_BGM, _bgmIsOn);
		}
	}

	private int _sfxIsOn;
	public bool sfxIsOn{
		get{
			if (PlayerPrefs.HasKey (SAVE_SFX)) {
				_sfxIsOn = PlayerPrefs.GetInt (SAVE_SFX);
			} else {
				_sfxIsOn = 1;
				PlayerPrefs.SetInt (SAVE_SFX, _sfxIsOn);
			}
			return (_bgmIsOn > 0);
		}
		set{
			if (value) {
				_sfxIsOn = 1;
			} else {
				_sfxIsOn = 0;
			}
			PlayerPrefs.SetInt (SAVE_SFX, _sfxIsOn);
		}
	}
		
	public int bestScore{
		get{
			if (PlayerPrefs.HasKey (SAVE_BESTSCORE))
				return PlayerPrefs.GetInt (SAVE_BESTSCORE);
			else
				return 0;
		}
	}
	
	public int lastScore{
		get{
			if (PlayerPrefs.HasKey (SAVE_LASTSCORE))
				return PlayerPrefs.GetInt (SAVE_LASTSCORE);
			else
				return 0;
		}
		set{
			if (value > bestScore) {
				PlayerPrefs.SetInt (SAVE_BESTSCORE, value);
			}
			PlayerPrefs.SetInt (SAVE_LASTSCORE, value);
		}
	}

	#endregion

}
