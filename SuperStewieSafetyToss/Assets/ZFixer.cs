﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ZFixer : MonoBehaviour {
	private float mult = 1f;
	void Update () {
		float _y = gameObject.transform.localPosition.y;
		gameObject.transform.localPosition = new Vector3 (
			gameObject.transform.localPosition.x,
			_y,
			_y * mult);
	}
}