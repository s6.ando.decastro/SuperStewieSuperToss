﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TrailMaker : MonoBehaviour {

	public Material material;

	public Transform[] pathPoints;

	public Sprite[] trailAssets;

	public Sprite arrow;
//	LineRenderer lineRenderer;

	private List<GameObject> trails;

	void Start () {
		Vector3[] curevedPoints = MakeSmoothCurve ( screenPositions(),  5.0f);

		trails = new List<GameObject> ();

		for (int i = 0; i < curevedPoints.Length; i++) {

			GameObject trail = Instantiate ( Resources.Load ("trail") ) as GameObject;
			if (i == curevedPoints.Length - 1)
				trail.GetComponent<SpriteRenderer> ().sprite = arrow;
			else
				trail.GetComponent<SpriteRenderer> ().sprite = trailAssets [i % 3];
			trail.transform.SetParent (this.transform);
			trails.Add ( trail );
		}
	}


	void Update () {
		Vector3[] curevedPoints = MakeSmoothCurve ( screenPositions(),  5.0f);

		for(int i = 0; i < trails.Count; i++){
			Vector3 pos = curevedPoints [i];
			trails [i].transform.position = pos;
		}
	}

	Vector3[] screenPositions(){
		List<Vector3> screenPoints = new List<Vector3> (pathPoints.Length);
		for(int i = 0; i < pathPoints.Length; i++){
			screenPoints.Add ( new Vector3(pathPoints[i].position.x, pathPoints[i].position.y,0) );
		}

		return (screenPoints.ToArray ());
	}

	Vector3[] MakeSmoothCurve(Vector3[] arrayToCurve, float smoothness){
		List<Vector3> points;
		List<Vector3> curvePoints;

		int pointsLength = 0;
		int curvedLength = 0;

		if (smoothness < 1.0f)
			smoothness = 1.0f;

		pointsLength = arrayToCurve.Length;


		curvedLength = (pointsLength * Mathf.RoundToInt(smoothness)) - 1;
		curvePoints = new List<Vector3> (curvedLength);

		float t = 0.0f;
		for (int pointInTimeCurve = 0; pointInTimeCurve < curvedLength + 1; pointInTimeCurve++) {
			t = Mathf.InverseLerp (0, curvedLength, pointInTimeCurve);

			points = new List<Vector3> (arrayToCurve);

			for (int j = pointsLength - 1; j > 0; j--) {
				for (int i = 0; i < j; i++) {
					points [i] = (1 - t) * points [i] + t * points [i + 1];
				}
			}

			curvePoints.Add (points[0]);
		}
		return( curvePoints.ToArray() );
	}
}
