﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Class to encapsulate audio data needed to play sounds
// Also keeps reference(s) to the AudioSource components(s) that will play the sound
[System.Serializable]
public class AudioData
{
	public string m_key = string.Empty;
	public AudioClip m_clip = null;
	public bool m_bIsLooping = false;
}

// Class used to manage and play the sounds
// Might be that we use different audio managers per state?
public class AudioManager : MonoBehaviour {

	public static AudioManager Instance = null;
	public int m_numBuffers = 16;

	private int m_currentBuffer = -1;

	//Possible Optimization 
	// if list/dictionary is huge, it might be good to use an algorithm to convert strings
	// into integers and use that instead of string comparisons
	public List<AudioData> m_audioLookup = new List<AudioData>();
	private Dictionary<string, AudioData> m_indexLookup = new Dictionary<string, AudioData>();

	private List<AudioSource> m_buffers = new List<AudioSource>();

	//-- BGM
	public AudioSource m_bgmSource;
	public AudioData[] m_bgms;
	private Dictionary<string, AudioData> m_bgmLookup = new Dictionary<string, AudioData>();

	void Awake () {
//		AudioManager.PlayBgm ();
		m_bgmSource = Camera.main.GetComponent<AudioSource> ();
		m_bgmSource.Play ();
		//-- BGM
		foreach (AudioData data in m_bgms) {
			m_bgmLookup[data.m_key] = data;
		}

		foreach( AudioData data in m_audioLookup ) {
			m_indexLookup[ data.m_key ] = data;
		}

		for( int i = 0; i < m_numBuffers; i++ ) {
			AudioSource source = this.gameObject.AddComponent<AudioSource>();
			source.playOnAwake = false;
			source.dopplerLevel = 0;
			source.loop = false;
			m_buffers.Add( source );
		}
	}

	void Start () {
		Instance = this;
	}

	void OnDestroy () {
		Instance = null;
	}

	private AudioSource GetNextBuffer () {
		int bufferIndex = -1;
		for( int i = 1; i <= m_numBuffers; i++ ) {
			int currentIndex = ( m_currentBuffer + i ) % m_numBuffers;
			if ( ! m_buffers[ currentIndex ].isPlaying ) {
				bufferIndex = currentIndex;
				break;
			}
		}
		
		// if null, force current buffer + 1
		if ( bufferIndex == -1 ) {
			bufferIndex = ( m_currentBuffer + 1 ) % m_numBuffers;
		}
		
		m_currentBuffer = bufferIndex;
		AudioSource source = m_buffers[ m_currentBuffer ];
		return source;
	}

	public static void PlayOneShot( string p_key, float p_vol = 1f ) {
		if (Instance == null || ! Instance.m_indexLookup.ContainsKey( p_key )) { return; }

//		if (!SUFSApplication.Instance.model.sfxIsOn)
//			return;

		AudioData data = Instance.m_indexLookup[ p_key ];
		AudioSource source = Instance.GetNextBuffer();
		source.PlayOneShot( data.m_clip );
		source.volume = p_vol;
	}

	public static void StopAllSFX(){
		if (Instance == null)
			return;
		for (int i = 0; i < Instance.m_numBuffers; i++) {
				AudioSource source = Instance.m_buffers[ i ];
				source.Stop();
		}
	}

	#region BGM
	public static void ChangeBGM(string p_key){
		if (Instance == null || 
		    Instance.m_bgmSource == null ||
		    ! Instance.m_bgmLookup.ContainsKey( p_key ) ) { return; }
		AudioData data = Instance.m_bgmLookup[ p_key ];
		Instance.m_bgmSource.clip = data.m_clip;
		Instance.m_bgmSource.Play ();
	}
	public static void PauseBgm(){
		if ( Instance == null || 
			Instance.m_bgmSource == null) { return; }
		Instance.m_bgmSource.Pause ();
	}
	public static void UnPauseBgm(){
		if ( Instance == null || 
			Instance.m_bgmSource == null) { return; }
		Instance.m_bgmSource.UnPause ();
	}
	public static void StopBgm(){
		if ( Instance == null || 
		    Instance.m_bgmSource == null) { return; }
		Instance.m_bgmSource.Stop ();
	}
	public static void PlayBgm(){
		if ( Instance == null || 
		    Instance.m_bgmSource == null) { return; }
		Instance.m_bgmSource.Play ();
	}
	#endregion

}
