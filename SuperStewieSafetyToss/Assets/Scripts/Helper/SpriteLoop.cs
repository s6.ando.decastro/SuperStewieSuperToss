﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SpriteLoop : MonoBehaviour {


	[SerializeField]
	private Sprite[] sprites;
	[SerializeField]
	private float FPS = 1f;

	private int curSprite = 0;
	private float counter = 0;
	private SpriteRenderer renderer;
	private Image image;

	public float startDelay = 0;
	public bool loop = true;

	void Start () {
		renderer = gameObject.GetComponent<SpriteRenderer> ();
		image = gameObject.GetComponent<Image> ();

		counter -= startDelay;
	}
	
	// Update is called once per frame
	void Update () {
		if (sprites.Length == 0)
			return;

		counter += Time.deltaTime;
		if (counter >= FPS) {
			counter -= FPS;

			curSprite++;
			if (curSprite >= sprites.Length) {
				if (!loop)
					gameObject.SetActive (false);
				curSprite = 0;
			}


			if(renderer)
				renderer.sprite = sprites [curSprite];
			else if(image)
				image.overrideSprite = sprites [curSprite];
		}
	}
}
