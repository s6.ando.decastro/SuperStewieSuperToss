﻿using UnityEngine;
using System.Collections;

public class KeyAnimator : MonoBehaviour {

	[SerializeField]
	private string keyAnimation;
	// Use this for initialization
	void Start () {
		gameObject.GetComponent<Animator> ().Play (keyAnimation);
	}
}
