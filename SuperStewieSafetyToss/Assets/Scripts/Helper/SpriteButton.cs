﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;


public class SpriteButton : MonoBehaviour {

	[Serializable]
	public class ButtonClickedEvent: UnityEvent{}

	[FormerlySerializedAs("onClick")]
	[SerializeField]
	private ButtonClickedEvent m_onClick = new ButtonClickedEvent();

	public bool isEnable = true;

	public ButtonClickedEvent onClick{
		get{ return m_onClick; }
		set{ m_onClick = value; }
	}
	
	void OnMouseDown(){

	}

	void OnMouseUp(){
		if(isEnable)
			m_onClick.Invoke ();
	}
}
