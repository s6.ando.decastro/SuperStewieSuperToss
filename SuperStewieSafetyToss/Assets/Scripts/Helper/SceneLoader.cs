﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

	static private int SceneToLoad = 1;
	static public void LoadScene(int p_sceneToLoad){
		SceneToLoad = p_sceneToLoad;
		SceneManager.LoadScene (0);
	}

	private bool loadScene = false;

	[SerializeField]
	private Text loadingText;
	
	// Update is called once per frame
	void Update () {
		if (loadScene == false) {
			loadScene = true;
			loadingText.text = "Loading...";
			StartCoroutine (LoadNewScene ());
		} else {
			loadingText.color = new Color (loadingText.color.r, loadingText.color.g, loadingText.color.b, Mathf.PingPong(Time.time, 1));
		}
	}

	IEnumerator LoadNewScene(){
		yield return new WaitForSeconds (3);

		AsyncOperation async = SceneManager.LoadSceneAsync (SceneToLoad);

		while(!async.isDone){
			yield return null;
		}
	}
}
