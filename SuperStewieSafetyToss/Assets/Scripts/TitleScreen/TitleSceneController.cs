﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TitleSceneController : MonoBehaviour {

	private float multiplier = Screen.height * 0.1f;

	public GameObject TitleHang;
	public GameObject MainHang;
	public GameObject ScoreHang;

//	[SerializeField]
//	private SpriteButton[] MainHangButtons;
//
//	[SerializeField]
//	private SpriteButton[] ScoreHangButtons;


	[SerializeField]
	private Button[] MainHangButtons;

	[SerializeField]
	private Button[] ScoreHangButtons;

	[SerializeField]
	private Text BestScore;
	[SerializeField]
	private Text LastScore;
	
	enum menuType{main, score} ;
	private menuType nextMenu = menuType.main;

	[SerializeField]
	private Transform show;
	[SerializeField]
	private Transform hide;

	void Start () {
		TitleMoveIn ();
//		MainHang.transform.position = hide.position;
		ScoreHang.transform.position = hide.position;
		Invoke ("ShowMainHang", 1f);
	}

	public void TriggerEvent(string command){

		switch(command){
		case "play":
			TitleMoveOut ();
			Invoke ("StartGame", 1f);
			break;
		case "bestScore":
			CloseMainHang ();
			Invoke ("ShowScoreHang", 0.5f);
			break;
		case "closeScoreHang":
			CloseScoreHang ();
			Invoke ("ShowMainHang", 0.5f);
			break;
		case "donate":
			Application.OpenURL ("https://www.stewietheduck.com/donationform.cfm");
			break;
		case "quack":
			//play quack sound
			Debug.Log("quack");
			break;
		default:
			break;

		}
	}

	private void StartGame(){
		SceneLoader.LoadScene (2);
	}


	#region TITLE
	private void TitleMoveIn(){
		
		iTween.MoveFrom (TitleHang, iTween.Hash(
			"y", TitleHang.transform.position.y + (10f * multiplier),
			"time", 1.0f, 
			"easetype",iTween.EaseType.easeOutBounce
		));
	}

	private void TitleMoveOut(){
		
		iTween.MoveBy (TitleHang, iTween.Hash(
			"y", 10f * multiplier,
			"time", 1.0f, 
			"easetype",iTween.EaseType.easeInOutBack
		));
	}
	#endregion

	#region MAIN HANG
	void ShowMainHang(){
		iTween.MoveTo (MainHang, iTween.Hash(
			"position", show.position,
			"time", 1.0f, 
			"easetype",iTween.EaseType.easeOutBounce
		));
		nextMenu = menuType.main;
		Invoke ("enableNextMenu", 1f);
	}

	private void CloseMainHang(){
		updateButtons (menuType.main, false);
		iTween.MoveTo (MainHang, iTween.Hash(
			"position", hide.position,
			"time", 1.0f, 
			"easetype",iTween.EaseType.easeInOutBack
		));
	}

	#endregion

	#region SCORE HANG

	void ShowScoreHang(){
		iTween.MoveTo (ScoreHang, iTween.Hash(
			"position", show.position,
			"time", 1.0f, 
			"easetype",iTween.EaseType.easeOutBounce
		));
		nextMenu = menuType.score;
		Invoke ("enableNextMenu", 1f);
	}

	void CloseScoreHang(){
		updateButtons (menuType.score, false);
		iTween.MoveTo (ScoreHang, iTween.Hash(
			"position", hide.position,
			"time", 1.0f, 
			"easetype",iTween.EaseType.easeInOutBack
		));
	}
	#endregion

	#region BUTTONS
	private void enableNextMenu(){
		updateButtons (nextMenu, true);
	}

	private void updateButtons(menuType type, bool enable){
		if (type == menuType.main) {
			for (int i = 0; i < MainHangButtons.Length; i++) {
				MainHangButtons [i].interactable = enable;
			}
		}
		else if (type == menuType.score) {
			for (int i = 0; i < ScoreHangButtons.Length; i++) {
				ScoreHangButtons [i].interactable = enable;
			}
		}
	}
	#endregion
}
