﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Timer : MonoBehaviour {
	
	public enum TimerType
	{
		Coundown,
		Countup
	};

	public TimerType m_timerType = TimerType.Coundown;
	private bool m_istimerStarts;

	public float m_targetTime = 0;
	private float m_time = 0;
	private float m_percentage = 0;
	public Image m_bar = null;
	public Text m_text =null;

	// Use this for initialization
	void Start (){
		timerStart ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!m_istimerStarts)
			return;

		m_time += Time.deltaTime;

		if (m_timerType == TimerType.Coundown) {
			if (m_time > m_targetTime)
				m_time = m_targetTime;

			m_percentage = m_time / m_targetTime;

			if (m_bar != null)
				m_bar.transform.localScale = new Vector3 (1f - m_percentage, 1f, 1f);
			if (m_text != null) {
				float _count = m_targetTime - m_time;
				string min = Mathf.Floor (_count / 60).ToString ("00");
				string sec = (_count % 60).ToString ("00");
				m_text.text = min + ":" + sec;
			}
				
			if (m_percentage == 1f)
				timeDone ();
		} else if (m_text != null) {
			string min = Mathf.Floor (m_time / 60).ToString("00");
			string sec = (m_time % 60).ToString ("00");
			m_text.text = min + ":" + sec;
		}
	}

	void timeDone() {
		//-- callback timer done here!!
		timerStop ();
//		App.controller.inGameUIController.Notify (Constants.Game1End);
//		AudioManager.PlayOneShot ("");
	}

	public void timerStart() {
		m_istimerStarts = true;
	}

	public void timerStop()
	{
		m_istimerStarts = false;
		m_time = 0;
	}

	public void timerPause (bool p_pause)
	{
		m_istimerStarts = !p_pause;
	}

	public void initTimer(float p_time, TimerType p_timerType) {
		timerStop ();
		m_targetTime = p_time;
		m_timerType = p_timerType;
	}

	public int getIntTime{
		get{
			if (m_timerType == TimerType.Coundown) {
				return (int)Mathf.Floor (m_targetTime - m_time);
			} else {
				return (int)Mathf.Floor (m_time);
			}
		}
	}
}
