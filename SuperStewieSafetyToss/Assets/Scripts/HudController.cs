﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HudController : MonoBehaviour {

	[SerializeField]
	private Text score;
	[SerializeField]
	private Text bestScore;

	void Start () {
		score.text = DataManager.Instance.currentScore.ToString ();
		bestScore.text = DataManager.Instance.bestScore.ToString ();
	}

	void Update () {
	
	}

	public void TriggerEvent(string command){
		switch(command){
		case "pause":
			break;
		default:
			break;
		}
	}
}
