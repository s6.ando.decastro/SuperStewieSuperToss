﻿using UnityEngine;
using System.Collections;

public class ShooterStewie : MonoBehaviour {

	#region VARIABLE
	private bool isFacingRight = true;

	private Vector3 bodyOrigPos;
	private ShooterControls shooter;

	[SerializeField]
	private GameObject body;
	[SerializeField]
	private GameObject vest;
	[SerializeField]
	private GameObject trail;

	#endregion

	protected virtual void Awake(){
		bodyOrigPos = body.transform.position;
		shooter = gameObject.GetComponent<ShooterControls> ();
//		shooter.SetTargetCreator (gameObject.GetComponent<TargetCreator>());
		shooter.targetCreator = gameObject.GetComponent<PathCreator>();
	}
	
	void Start () {
		trail.SetActive (false);
	}

	void Update () {
		if (shooter.toFaceRight != isFacingRight) {
			isFacingRight = shooter.toFaceRight;
			Vector3 scale = new Vector3 (shooter.toFaceRight ? 1 : -1,1,1);
			body.transform.localScale = scale;
			vest.transform.localScale = scale;
		}

		if (shooter.hasTouched && !trail.activeSelf) {
			trail.SetActive (true);
		} else if(!shooter.hasTouched && trail.activeSelf ){
			trail.SetActive (false);
		}
	}

	public void PutOut(){
		body.transform.position = new Vector3 (0,-8f,0);
	}

	public void MoveIn(){
		iTween.MoveTo (body, iTween.Hash(
			"position", bodyOrigPos,
			"time", 1.0f, 
			"easetype",iTween.EaseType.easeOutExpo
		));
	}
}
