﻿using UnityEngine;
using System.Collections;

public class ShooterControls : MonoBehaviour {


	private bool isTouchingRight = true;
	private bool touchDidBegan = false;
	private PathCreator _targetCretor;


	void Start () {
	
	}

	public PathCreator targetCreator { set{ _targetCretor = value; } }

	void Update () {

		if (Input.GetMouseButton (0)) {
			touchDidBegan = true;
			Vector3 _input = Input.mousePosition;
			RaycastHit _hit;
			Ray _ray;

			Vector3 _startPos = Camera.main.WorldToScreenPoint (transform.position);

			_ray = Camera.main.ScreenPointToRay (_input);
			Physics.Raycast (_ray, out _hit, 100f);

			isTouchingRight = _input.x > _startPos.x;

			//-- hit position
			//_hit.point
			_targetCretor.SetTarget(_hit.point);
		} else if (Input.GetMouseButtonUp (0)) {
			touchDidBegan = false;
		}
	}

	public bool toFaceRight{ get{ return isTouchingRight; } }
	public bool hasTouched{ get{ return touchDidBegan; } }
}
