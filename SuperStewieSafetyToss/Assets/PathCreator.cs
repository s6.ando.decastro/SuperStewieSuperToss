﻿using UnityEngine;
using System.Collections;

public class PathCreator : MonoBehaviour {

	[SerializeField]
	private Transform[] wayPoint;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetTarget(Vector3 p_target){
		wayPoint [3].position = p_target;

		Vector3 _dir = wayPoint [3].position - wayPoint [0].position;
		_dir.Normalize ();
		float _dis = Vector3.Distance (wayPoint [0].position, wayPoint [3].position);
		wayPoint [1].position = (wayPoint [0].position + (_dir * _dis * 0.4f)) + new Vector3 (0, _dis * 0.7f, 0);
		wayPoint [2].position = (wayPoint [0].position + (_dir * _dis * 0.8f)) + new Vector3 (0, _dis * 0.5f, 0);
	}
}
