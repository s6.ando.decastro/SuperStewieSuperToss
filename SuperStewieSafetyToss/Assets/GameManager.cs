﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public ShooterStewie Stewie;

	void Start () {
		DataManager.Instance.resetToStart ();
		Stewie.PutOut ();
		StartGame ();
	}

	void StartGame(){
		Stewie.MoveIn ();
	}

	void Update () {
		
	}
}
